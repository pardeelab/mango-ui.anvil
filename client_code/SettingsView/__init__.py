from ._anvil_designer import SettingsViewTemplate
from anvil import *
import anvil.server

class SettingsView(SettingsViewTemplate):
  def __init__(self, proxy, **properties):
    # Set Form properties and Data Bindings.
    self.init_components(**properties)
    
    self.proxy = proxy
    self.proxy.add_handler('response', self.on_response)
    
  def file_loader_credentials_change(self, file, **event_args):
    """Handler called when a user uploads a new credentials file."""
    self.file_loader_credentials.clear()

    # Send new credentials to the raspberry pi.
    self.proxy.send_command('set_google_credentials', file.get_bytes().decode('utf-8'))

    # Request the google client email.
    self.proxy.send_command('get_google_client_email')
    

  def on_response(self, response):
    """Message handler for the 'response' subtopic (i.e., responses
    to remotely executed commands).
    
    If you need a return value from a remoteley executed command, add
    a handler for the 'response' subtopic. Each response includes the
    command name and a command `id`, which is a unique id returned by
    every call to `send_command`.
    
    Parameters
    ----------
    response : dict
        Response from a remote command. Should contain at least the
        following keys:
          'command': name of the remote function called.
          'id': unique command id of the issued command.
          'return': value returned by the function.
    """
    if response['command'] == 'get_google_client_email':
      self.label_google_client_email.text = "Google client email: " + response['return']
      
    if response['command'] == 'get_google_drive_folder_id':
      self.folder_id_label.text = "Folder ID: " + response['return']

  def credentials_remove_clicked(self, **event_args):
    """This method is called when the button is clicked"""

    # Send new credentials to the raspberry pi.
    self.proxy.send_command('set_google_credentials', '')
    # Get the google client email.
    self.proxy.send_command('get_google_client_email')

  def folder_id_textbox_pressed_enter(self, **event_args):
    """This method is called when the user presses Enter in this text box"""
    pass

  def folder_id_button_click(self, **event_args):
    """This method is called when the button is clicked"""
    self.proxy.send_command('set_google_drive_folder_id', str(self.folder_id_textbox.text))
    self.proxy.send_command('get_google_drive_folder_id')

  def text_fluorescence_textbox_pressed_enter(self, **event_args):
    """This method is called when the user presses Enter in this text box"""
    pass

  def button_fluroescence_button_click(self, **event_args):
    """This method is called when the button is clicked"""
    self.proxy.send_command('set_fluorescence_threshold', str(self.fluorescence_textbox.text))
    self.proxy.send_command('get_fluorescence_threshold')
    pass




