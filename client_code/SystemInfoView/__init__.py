from ._anvil_designer import SystemInfoViewTemplate
from anvil import *
import anvil.server

class SystemInfoView(SystemInfoViewTemplate):
  def __init__(self, **properties):
    # Set Form properties and Data Bindings.
    self.init_components(**properties)

    # Any code you write here will run when the form opens.
    self.update()
    
  def update(self):
    """Update the page."""
    device_dict = anvil.server.call('get_connected_devices')
    label = ""
    for address, name in device_dict.items():
      label += '%s: %s\n' % (address, name)
    self.label_connected_devices.text = label

  def refresh_button_click(self, **event_args):
    """Handler called when the refresh button is clicked."""
    self.update()
