from ._anvil_designer import StepViewTemplate
from anvil import *
import datetime


class StepView(StepViewTemplate):
  def __init__(self, **properties):
    # Set Form properties and Data Bindings.
    self.init_components(**properties)
    
    # Register a handler for the `x-enable` event. This allows all of the step widgets
    # to be enabled/disabled at once.
    self.set_event_handler('x-enable', self.enable_step)
    
    # Time to wait for subsequent changes before updating the config file.
    self.update_debounce = 1

  @property
  def proxy(self):
    return self.parent.parent.proxy
    
  def run_step_button_click(self, **event_args):
    """This method is called when the 'run step' button is clicked"""
    print('%s: pump=%s, pulses=%s, period=%.1f' % (datetime.datetime.now(), self.item['pump'],
                                                   self.item['pulses'], self.item['period']))
    self.parent.parent.force_config_update()
    self.proxy.send_command('run_step', self.item['index'])
  
  def radio_button_clicked(self, **event_args):
    """This method is called when the step's radio button is selected"""
    self.proxy.set_property('step_index', self.item['index'])
    
  def enable_step(self, **event_args):
    """This method is called when the step receives an 'x-enable' event"""
    self.radio_button.enabled = event_args['enabled']
    self.run_step_button.enabled = event_args['enabled']
    self.pulses_up_button.enabled = event_args['enabled']
    self.pulses_down_button.enabled = event_args['enabled']
    self.period_up_button.enabled = event_args['enabled']
    self.period_down_button.enabled = event_args['enabled']
    
  def pulses_up_button_click(self, **event_args):
    """This method is called when the 'pulses up' button is clicked"""
    self.item['pulses'] = self.item['pulses'] + 1
    self.pulses.text = self.item['pulses']
    self.parent.parent.update_config_timer.interval = self.update_debounce

  def pulses_down_button_click(self, **event_args):
    """This method is called when the 'pulses down' button is clicked"""
    if self.item['pulses'] > 1:
      self.item['pulses'] -= 1
      self.pulses.text = self.item['pulses']
      self.parent.parent.update_config_timer.interval = self.update_debounce
  
  def period_up_button_click(self, **event_args):
    """This method is called when the 'period up' button is clicked"""
    if self.item['period'] >= 2:
      self.item['period'] += 1
    else:
      self.item['period'] += 0.1
    self.parent.parent.update_config_timer.interval = self.update_debounce
    self.update_period()

  def period_down_button_click(self, **event_args):
    """This method is called when the 'period down' button is clicked"""
    if self.item['period'] > 2:
      self.item['period'] -= 1
    elif self.item['period'] >= 0.3:
      self.item['period'] -= 0.1
    self.parent.parent.update_config_timer.interval = self.update_debounce
    self.update_period()

  def update_period(self):
    """Update the decimal place formatting of the step period depending on it's value."""
    if self.item['period'] >= 2:
      self.period.text = '%d' % self.item['period']
    else:
      self.period.text = '%.1f' % self.item['period']
    self.item['period'] = round(float(self.period.text), 1)

  def text_box_1_pressed_enter(self, **event_args):
    """This method is called when the user presses Enter in this text box"""
    pass

  def threshold_set_button_click(self, **event_args):
    """This method is called when the button is clicked"""
    pass

  def threshold_remove_button_click(self, **event_args):
    """This method is called when the button is clicked"""
    pass



