import math
from ._anvil_designer import ProtocolViewTemplate
from anvil import *
import anvil.server
import anvil.media
import json

class ProtocolView(ProtocolViewTemplate):
  def __init__(self, proxy, **properties):
    # Set Form properties and Data Bindings.
    self.init_components(**properties)
    
    self.status_label.text = ''
    
    # Add handlers for the mqtt topics we are subscribing to
    self.proxy = proxy
    self.proxy.add_handler('state', self.on_state_updated)
    self.proxy.add_handler('config', self.on_config_updated)
    self.proxy.add_handler('log', self.on_log_updated)

  def on_exception(self, data):
    """Message handler for the 'exception' subtopic."""
    alert(data['message'])
      
  def on_state_updated(self, state):
    """Message handler for the 'state' subtopic.
    
    Parameters
    ----------
    state : dict
        Dictionary of changed state values.
    """
    if 'step_index' in state.keys():
      # The step index has changed -> set the radio button to the current step.
      self.steps_panel.get_components()[self.proxy.step_index].radio_button.selected = True
      
    # If the protocol or a step are running, disable all buttons that can modify the
    # config and update the current runnning status.
    if self.proxy.protocol_running or self.proxy.seconds_remaining_in_step > 0:
      self.steps_panel.raise_event_on_children('x-enable', enabled=False)
      self.file_loader_protocol.enabled = False
      self.download_protocol_button.enabled = False
      self.play_button.icon = 'fa:stop'
      
      step = self.steps_panel.items[self.proxy.step_index]
      seconds = self.proxy.seconds_remaining_in_step % 60
      minutes = math.floor(self.proxy.seconds_remaining_in_step % 3600 / 60)
      hours = math.floor(self.proxy.seconds_remaining_in_step / 3600)

      if hours > 0:
        self.status_label.text = f'{step["label"]} ({hours}h{minutes}m{seconds}s)'
      elif minutes > 0:
        self.status_label.text = f'{step["label"]} ({minutes}m{seconds}s)'
      else:
        self.status_label.text = f'{step["label"]} ({seconds}s)'
    else:
        self.steps_panel.raise_event_on_children('x-enable', enabled=True)
        self.file_loader_protocol.enabled = True
        self.download_protocol_button.enabled = True
        self.status_label.text = ''
        self.play_button.icon = 'fa:play'
        
  def on_config_updated(self, config):
    """Message handler for the 'config' subtopic.
    
    Parameters
    ----------
    config : dict
        Dictionary of changed config values.
    """
    steps = self.proxy.config['steps']
    for i in range(len(steps)):
        steps[i]['index'] = i

    # Assign the steps dictionary to the self.steps_panel.items variable. This has
    # updates the steps panel widgets automatically thanks to anvil's data bindings.
    self.steps_panel.items = steps
    
    # Update the period text (adjusts the number of decimal places depending on the
    # value).
    for component in self.steps_panel.get_components():
      component.update_period()
    
    # If the step index is no longer valid, set it to 0.
    if self.proxy.step_index < len(steps):
      self.proxy.step_index = 0

    # Set the radio button for the current step.
    self.steps_panel.get_components()[self.proxy.step_index].radio_button.selected = True

  def on_log_updated(self, update):
    """Message handler for the 'log' subtopic.
    
    Parameters
    ----------
    update : dict
        Dictionary of new log values.
    """
    if 'temperature' in update.keys():
      self.temperature = update['temperature']
      if self.temperature is None:
        self.temperature_label.text = 'Temperature: ?'
      else:
        self.temperature_label.text = 'Temperature: %s°C' % self.temperature

    if 'fluorescence' in update.keys():
      self.rfu = '%d' % update['fluorescence']['0']
      self.fluorescence_label.text = 'RFU (470nm/510nm): %s' % self.rfu

  def file_loader_protocol_change(self, file, **event_args):
    """Handler called when a user uploads a new config file."""
    self.file_loader_protocol.clear()

    # Load config dictionary from the uploaded json file
    config = json.loads(file.get_bytes().decode('utf-8'))
    self.update_config(config)
  
  def update_config(self, config=None):
    """Update the config file on the Raspberry Pi.
    
    Parameters
    ----------
    config : dict
        Configuration dictionary.
    """
    if config == None:
      config = self.proxy.config
    
    # Upload the new config to the server.
    self.proxy.send_command('set_config', config)
      
  def play_button_click(self, **event_args):
    """This method is called when the 'play' or 'stop' button are clicked"""
    if self.play_button.icon == 'fa:play':
      # Force any pending changes to the config before running.
      self.force_config_update()
      self.proxy.send_command('run_protocol')
    else:
      self.proxy.send_command('pause_protocol')

  def download_protocol_button_click(self, **event_args):
    """This method is called when the 'download' button is clicked"""
    self.force_config_update()
    config_file = anvil.BlobMedia('text/plain', json.dumps(self.proxy.config, indent=2).encode(), name='config.json')
    anvil.media.download(config_file)

  def update_config_timer_tick(self, **event_args):
    """This method is called Every [interval] seconds. Does not trigger if [interval] is 0.
    
    This timer is initialized whenever the configuration is changed. If another change occurs
    before the timer goes off, the timer gets reset. This prevents sending multiple updates if,
    for example, the user is repeatedly clicking one of the up/down buttons.
    """
    self.update_config()
    self.update_config_timer.interval = 0

  def force_config_update(self):
    """Force an update of the configuration if there are pending changes."""

    # If the configuration has changed since the last update,
    # self.update_config_timer.interval will be set to > 0.
    if self.update_config_timer.interval > 0:
      
      # Stop the timer
      self.update_config_timer.interval = 0
      
      # Update the config
      self.update_config()
