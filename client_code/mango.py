from .mqtt_proxy import MqttProxyBase

class MqttProxy(MqttProxyBase):
  def __init__(self, uuid):
    super().__init__('mango' + '/' + uuid)

    # Initialize state, config and log
    self.state = {
        'protocol_running': False,
        'step_index': 0,
        'seconds_remaining_in_step': 0
    }
    self.config = self.config = {'steps': []}
    self.log = {
      'temperature': {
        'utc_timestamp': [],
        'temperature': [],
      },
      'fluorescence': {
        'utc_timestamp': [],
        '0': [],
        '1': [],
      }
    }
    
    self.add_handler('state', self.on_state_updated)
    self.add_handler('config', self.on_config_updated)      
    self.add_handler('log', self.on_log_updated)
    self.add_handler('response', self.on_response)

  def on_connection(self, response):
    """Handler called when a connection to the MQTT broker is established."""
    super().on_connection(response)
    
    # Request an update of the state and config.
    self.send_command('request_config')
    self.send_command('request_state')
    self.send_command('request_google_client_email')
    self.send_command('get_google_drive_folder_id')

  def on_state_updated(self, state):
    """Message handler for the 'state' subtopic.
    
    Parameters
    ----------
    state : dict
        Dictionary of changed state values.
    """
    # Update the local state dictionary
    self.state.update(state)

  def on_config_updated(self, config):
    """Message handler for the 'config' subtopic.
    
    Parameters
    ----------
    config : dict
        Dictionary of changed config values.
    """
    # Update the local state dictionary
    self.config.update(config)

  def on_log_updated(self, update):
    """Message handler for the 'log' subtopic.
    
    Parameters
    ----------
    update : dict
        Dictionary of new log values.
        
        The 'utc_timestamp' key should be present in all updates.
    """
    utc_timestamp = update.pop('utc_timestamp')
    for log_key, data in update.items():
      if type(data) is dict:
        for k, v in data.items():
          self.log[log_key][k].append(v)
          self.log[log_key]['utc_timestamp'].append(utc_timestamp)
      else:
        self.log[log_key][log_key].append(data)
        self.log[log_key]['utc_timestamp'].append(utc_timestamp)

  def on_response(self, response):
    """Message handler for the 'response' subtopic (i.e., responses
    to remotely executed commands).
    
    If you need a return value from a remoteley executed command, add
    a handler for the 'response' subtopic. Each response includes the
    command name and a command `id`, which is a unique id returned by
    every call to `send_command`.
    
    Parameters
    ----------
    response : dict
        Response from a remote command. Should contain at least the
        following keys:
          'command': name of the remote function called.
          'id': unique command id of the issued command.
          'return': value returned by the function.
    """
    pass
    
  def update_state(self, **state):
    self.state.update(state)
  
  @property
  def step_index(self):
      return self.state['step_index']

  @step_index.setter
  def step_index(self, index):
      self.update_state(step_index=index, seconds_remaining_in_step=0)

  @property
  def protocol_running(self):
      return self.state['protocol_running']

  @protocol_running.setter
  def protocol_running(self, value):
      self.update_state(protocol_running=value)

  @property
  def seconds_remaining_in_step(self):
      return self.state['seconds_remaining_in_step']

  @seconds_remaining_in_step.setter
  def seconds_remaining_in_step(self, value):
      self.update_state(seconds_remaining_in_step=value)