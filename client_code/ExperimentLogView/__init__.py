from ._anvil_designer import ExperimentLogViewTemplate
from anvil import *
import plotly.graph_objects as go
import anvil.server
import time

class ExperimentLogView(ExperimentLogViewTemplate):
  def __init__(self, proxy, **properties):
    # Set Form properties and Data Bindings.
    self.init_components(**properties)

    # Any code you write here will run when the form opens.
    self.start_time = time.time()
    
    # Add handlers for the mqtt topics we are subscribing to
    self.proxy = proxy
    self.proxy.add_handler('log', self.build_plot)
    self.build_plot()
    
  def style_plot(self, plot):
    plot.layout = go.Layout(
                            # expand the graphs
                            margin=dict(
                                l=50, #left margin
                                r=50, #right margin
                                b=50, #bottom margin
                                t=50, #top margin
                            ),
                            font=dict(family='Noto Sans', size=10),
                            # Format x-axis
                            xaxis=dict(
                              zeroline=False,
                              tickfont=dict(
                                  family='Noto Sans',
                                  size=11,
                                  color='#808080'
                              ),
                            ),
                            # Format y-axis
                            yaxis=dict(
                                zeroline=False,
                                tickfont=dict(
                                    family='Noto Sans',
                                    size=11,
                                    color='#808080'
                                ),
                            )
                          )

  def build_plot(self, update=None):
    # Create a Scatter plot with this data, and change the colour of the line
    self.plot_1.data = go.Scatter(y=self.proxy.log['temperature']['temperature'],
                                  x=[t - self.start_time for t in self.proxy.log['temperature']['utc_timestamp']], 
                                  line=dict(color='#2196f3'))
    self.plot_1.layout = {
      'title': "Temperature",
      'xaxis': {'title': 'Time (s)'},
      'yaxis': {'title': 'Temperature'},
          # Scatter plot with the fluorescence
    }
    self.plot_2.data = [
      go.Scatter(x=[t - self.start_time for t in self.proxy.log['fluorescence']['utc_timestamp']], line=dict(color='#2196f3'), 
                 y=self.proxy.log['fluorescence']['0']),
      go.Scatter(x=[t - self.start_time for t in self.proxy.log['fluorescence']['utc_timestamp']], line=dict(color='00008B'), 
                 y=self.proxy.log['fluorescence']['1'])
    ]
   

    self.plot_2.layout = {
      'title': "fluorescence",
      'xaxis': {'title': 'Time (s)'},
      'yaxis': {'title': 'fluorescence'},
    }
    